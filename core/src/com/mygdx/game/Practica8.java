package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class Practica8 extends ApplicationAdapter implements InputProcessor {
	TiledMap tiledMap;
	OrthographicCamera camera;
	ParticleEffect pe;
	SpriteBatch batch;

	TiledMapRenderer tiledMapRenderer;
	public void create () {
		batch = new SpriteBatch();
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,w,h);
		camera.update();
		tiledMap = new TmxMapLoader().load(Gdx.files.internal("tileset/mapa.tmx").toString());
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
		Gdx.input.setInputProcessor(this);

		pe = new ParticleEffect();
		pe.load(Gdx.files.internal("particles/rainParticle"),Gdx.files.internal("particles"));
		pe.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
		pe.start();
	}

	@Override

	public void render () {

		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();

		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();

		batch.begin();
		pe.draw(batch, Gdx.graphics.getDeltaTime());
		batch.end();

		if (pe.isComplete()) pe.reset();

	}

	@Override

	public boolean keyDown(int keycode) {

		return false;

	}

	@Override

	public boolean keyUp(int keycode) {

		if(keycode == Input.Keys.LEFT)

			camera.translate(-32,0);

		if(keycode == Input.Keys.RIGHT)

			camera.translate(32,0);

		if(keycode == Input.Keys.UP)

			camera.translate(0,-32);

		if(keycode == Input.Keys.DOWN)

			camera.translate(0,32);

		if(keycode == Input.Keys.NUM_1)

			tiledMap.getLayers().get(0).setVisible(

					!tiledMap.getLayers().get(0).isVisible());

		if(keycode == Input.Keys.NUM_2)

			tiledMap.getLayers().get(1).setVisible(

					!tiledMap.getLayers().get(1).isVisible());


		if(keycode == Input.Keys.NUM_3)
			pe.scaleEffect(0.8f);

		if(keycode == Input.Keys.NUM_4)
			pe.scaleEffect(1.2f);

		return false;

	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		pe.setPosition(screenX, Gdx.graphics.getHeight()-screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
